## Outward SideLoader

The Outward SideLoader is an API and a mod development toolkit for creating and editing custom content.

Please view the **[SideLoader Docs](https://sinai-dev.github.io/OSLDocs/#/)** for full documentation on SideLoader.

### Latest changes

#### 3.5.5

* Actually fixed bug with SkillTrees which have no breakthrough

#### 3.5.4

* Fixed a bug with SkillTrees which don't have any breakthroughs

#### 3.5.3

* Fixed a bug with `SL_SkillTree` not setting skills above breakthrough line to require the breakthrough

### Installation (manual)

For a manual installation, do the following

1. Create the folder(s): `Outward\BepInEx\plugins\SideLoader\`.
2. Extract the archive into a folder. **Do not extract into the Outward game folder.**
3. Move the contents of the `plugins\` folder from the archive into the `BepInEx\plugins\SideLoader` folder you created.
3. It should look like `Outward\BepInEx\plugins\SideLoader\SideLoader.dll`, etc
4. Launch the game, you should see a new keybinding in your in-game keybindings for the SideLoader menu.