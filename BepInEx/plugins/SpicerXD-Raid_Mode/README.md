﻿What Raid Mode does:

* Let's up to 20 players join an online game!

* Expands on the vanilla co-op difficulty scaling for more players!

* Fixes many co-op bugs!

* Allows split screen online!

Notice: Everyone playing in your game needs the mod installed for it to work correctly.

Secret Feature: If you want to configure the player limit or the difficulty scaling. You can write settings at the end of the room name when creating an online lobby.
For example, naming your online lobby "RoomName =2 *4", sets the player limit to 2 and forces the difficulty scaling to always behave as if there is 4 players in the game. (The spaces between each option is required.)


Best of luck out there, adventurers!
If you encounter any bugs, please report them here and list any other mods you may have.

A very special shoutout to Sinai for his amazing help making this mod Plus Ultra!
And another equally special shoutout to all my good friends for their big help as bug testers! You know who you are! <3