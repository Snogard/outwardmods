## Item Price Helper

Update to https://www.nexusmods.com/outward/mods/186 by Kekasi / Saeculum.

### Latest changes

#### 2.1.2

* Renamed Keybind

#### 2.1.1

* Custom Key now toggles value display type between silver / lb and sell price. Hold on inventory open was dumb. Sorry.
* Fixed shop prices displaying incorrectly

#### 2.1.0

* Removed Config
* Displays Sell Price by default
* Hold Custom Key on inventory open to view Silver/Lb. (Swapping value formats live coming in 2.2.0)

#### 2.0.1

* Missing Texture Fix

#### 2.0.0

* Thunderstore Upload
* No longer requires Shared Mod Config Menu. Uses Outward Config Manager by Mefino instead.

#### 1.0.0

* https://www.nexusmods.com/outward/mods/186 by Kekasi / Saeculum.