## Shared Stashes

Simply shares the contents of your Stash inventories between all player houses (and the New Sirocco stash). Works with existing saves, no setup required.

### Manual install

To install manually, simply put the `SharedStashes.dll` file in `Outward\BepInEx\plugins\`